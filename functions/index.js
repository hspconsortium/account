const functions = require('firebase-functions');

const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);


exports.deleteProfileInfo = functions.auth.user().onDelete(event => {
  const user = event.data;
  const uid = user.uid;
  const email = user.email;

  return deleteProfileInfo(uid, email);
});


function deleteProfileInfo(uid, email) {
  console.log('Deleting user ' + uid + ' with email: ' + email);
  return admin.database().ref('users/' + uid).remove();
}
