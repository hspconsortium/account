#!/usr/bin/env bash

docker build -t account/node-web-app .
docker run -p 8065:8065 account/node-web-app
