var fs = require('fs');

const env = process.argv[2].substring(2);
console.log("Copying " + env + " config...");
fs.createReadStream('./src/config/config-' + env + '.js').pipe(fs.createWriteStream('./src/config/config.js'));

