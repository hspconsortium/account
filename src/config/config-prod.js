let firebase = {
  apiKey: "AIzaSyA9Id72GPbTyWbEkyEwkyf1Z_wOJwk0bHc",
  authDomain: "hspc-3f7ad.firebaseapp.com",
  databaseURL: "https://hspc-3f7ad.firebaseio.com",
  projectId: "hspc-3f7ad",
  storageBucket: "hspc-3f7ad.appspot.com",
  messagingSenderId: "235269118781"
};

let app = {
  personaAuthEndpoint: "https://sandbox-api.logicahealth.org/userPersona/authenticate",
  authActionFrameBaseUrl: "https://hspc-3f7ad.firebaseapp.com/__/auth/action",
  accountApiUrl: "https://account-api.logicahealth.org"
};

export default {firebase, app}
