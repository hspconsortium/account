import React from "react";
import PropTypes from "prop-types";
import {TextField, RaisedButton, FlatButton} from "material-ui";
import muiThemable from 'material-ui/styles/muiThemeable';
import {Link, Route} from 'react-router-dom';

class ResetPassword extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      email: "",
      errors: {},
      authenticating: false,
      sent: false
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.handleEmailError = this.handleEmailError.bind(this);
    this.handleEmailSuccess = this.handleEmailSuccess.bind(this);
  }

  onChange(event) {
    if (event.target.name === 'email')
      this.setState({email: event.target.value});
  }

  onSubmit(event) {
    this.setState({authenticating: true});
    this.props.firebase.auth().sendPasswordResetEmail(this.state.email)
      .then(this.handleEmailSuccess)
      .catch(this.handleEmailError);
  }

  handleEmailSuccess() {
    this.setState({authenticating: false, email: "", sent: true});
  }

  handleEmailError(error) {
    this.setState({authenticating: false});
    console.error(error);
    if (error.code === 'auth/invalid-email') {
      this.setState({
        errors: {
          email: error.message
        }
      });
    } else if (error.code === 'auth/user-not-found') {
      this.setState({
        errors: {
          email: error.message
        }
      });
    }
  }

  FirstTimeLoginComponent = (props) => {
    return (
      <div style={{color: props.muiTheme.palette.textColor}}>
        <h3>HPSC Has a New and Improved Account Management System</h3>
        <p>This first time logging in, you'll need to <strong>set a new password</strong> using your existing account.
        </p>
        <ul>
          <li>Don't worry, your sandbox(es) are still there.</li>
          <li>If you run into any trouble, we're here to help. Email us at <a style={{color: this.props.muiTheme.palette.accent3Color}} href="support@logicahealth.org.">support@logicahealth.org.</a>
          </li>
        </ul>
      </div>
    );
  };


  render() {

    if (this.state.sent)
      return (
        <div style={{color: this.props.muiTheme.palette.textColor}}>
          <h2>Email Sent</h2>
          <p>Follow the instructions in the email to reset your password.</p>
          <RaisedButton primary={true} fullWidth={true} label="Return to login page" containerElement={<Link to="/annon"/>}/>
        </div>
      );

    return (
      <div>
        <Route path="/annon/reset-password/first-time" render={() => <this.FirstTimeLoginComponent muiTheme={this.props.muiTheme} />}/>
        <TextField
          floatingLabelText="Email"
          value={this.state.email}
          onChange={this.onChange}
          name="email"
          errorText={this.state.errors.email}
          fullWidth={true}
        />
        <br/>
        <br/>
        <RaisedButton
          label={this.authenticating ? 'Sending...' : 'Send Password Reset Email'}
          onClick={this.onSubmit}
          disabled={this.state.authenticating}
          fullWidth={true}
          primary={true}
        />
        <br/>
        <br/>
        <FlatButton label="Return to login page" primary={this.state.sent} containerElement={<Link to="/annon"/>}/>
      </div>
    )
  };
}

ResetPassword.propTypes = {
  firebase: PropTypes.object.isRequired
};

ResetPassword.defaultProps = {};

export default muiThemable()(ResetPassword);
