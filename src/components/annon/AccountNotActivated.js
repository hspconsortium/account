import React from 'react';
import {RaisedButton} from 'material-ui';
import PropTypes from 'prop-types';

function AccountNotActivated({emailSent, sendActivationEmail}) {
  return (
    <div className="annon-content">
      <h1>Awaiting Activation</h1>
      <p>Check your email for a link to activate your HSPC account.</p>
      <RaisedButton
        label={emailSent ? 'Check your inbox' : 'Re-Send Email'}
        onClick={sendActivationEmail}
        disabled={emailSent}
        fullWidth={true}
        primary={true}
      />
    </div>
  );
}

AccountNotActivated.propTypes = {
  emailSent: PropTypes.bool.isRequired,
  sendActivationEmail: PropTypes.func.isRequired
};

export default AccountNotActivated;
