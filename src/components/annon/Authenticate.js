import React from "react";
import {Redirect, Link} from 'react-router-dom';
import PropTypes from "prop-types";
import * as Firebase from "firebase";
import {Client} from 'node-rest-client';
import Cookies from 'universal-cookie';
import {Paper, Snackbar, Tab, Tabs} from "material-ui";
import muiThemeable from 'material-ui/styles/muiThemeable';
import LoginForm from "./LoginForm";
import CreateAccountForm from "./CreateAccountForm";
import config from "../../config/config";

import "./Authenticate.css";


class Authenticate extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      displayName: "",
      authenticating: false,
      notificationMessage: "",
      notificationOpen: false,
      errors: {},
      selectedTab: 'login',
      newsletter1: true,
      newsletter2: true
    };

    this.onLogIn = this.onLogIn.bind(this);
    this.onCreateAccount = this.onCreateAccount.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onLogInWithGoogle = this.onLogInWithGoogle.bind(this);
    this.onCreateAccountWithGoogle = this.onCreateAccountWithGoogle.bind(this);
    this.handleAuthError = this.handleAuthError.bind(this);
    this.handleCreateError = this.handleCreateError.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.redirectIfNeeded = this.redirectIfNeeded.bind(this);

    this.firebaseGoogle = new Firebase.auth.GoogleAuthProvider();
  }

  handleKeyPress = (e) => {
    if (e.key === 'Enter') {
      if (this.state.selectedTab === 'login')
        this.onLogIn();
      else
        this.onCreateAccount();
    }
  };

  onChange(event) {
    if (event.target.name === 'email')
      this.setState({email: event.target.value, notificationOpen: false});
    else if (event.target.name === 'password')
      this.setState({password: event.target.value, notificationOpen: false});
    else if (event.target.name === 'displayName')
      this.setState({displayName: event.target.value});
    else if (event.target.name === 'newsletter1')
      this.setState({newsletter1: !this.state.newsletter1});
    else if (event.target.name === 'newsletter2')
      this.setState({newsletter2: !this.state.newsletter2});
  }

  onLogIn() {
    this.setState({authenticating: true});
    this.handlePersonaLogin();
  }

  handlePersonaLogin() {
    if (this.props.afterAuthRedirect && this.state.email && this.looksLikePersona(this.state.email)) {
      let client = new Client();
      let args = {
        data: {
          username: this.state.email,
          password: this.state.password
        },
        headers: {"Content-Type": "application/json"}
      };
      client.post(config.app.personaAuthEndpoint, args, (data, response) => {
        if (response.statusCode !== 200 || !data.jwt || data.jwt.length < 1) {
          this.handleFirebaseLogin();
          return;
        }
        const cookies = new Cookies();
        cookies.set('hspc-persona-token', data.jwt, {
          domain: this.props.topLevelDomain,
          path: '/',
          expires: new Date((new Date()).getTime() + 3 * 60000)
        });
        window.location = this.props.afterAuthRedirect;
      });
    } else {
      this.handleFirebaseLogin();
    }
  }

  looksLikePersona(username) {
    let personaRegex = /^[a-zA-Z0-9]{1,50}@[a-zA-Z0-9]{1,20}$/;
    return username.match(personaRegex) !== null;
  }

  redirectIfNeeded() {
    if (this.props.afterAuthRedirect && this.props.firebase.auth().currentUser.emailVerified)
      window.location = this.props.afterAuthRedirect;
  }

  handleFirebaseLogin() {
    this.props.firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password)
      .then(() => {
        this.redirectIfNeeded();
      })
      .catch(this.handleAuthError);
  }

  onCreateAccount() {
    // validate DisplayName
    if (!this.state.displayName || !this.state.displayName.length) {
      this.setState({
        errors: {
          displayName: "Please enter your full name"
        }
      });
      return;
    }
    this.setState({authenticating: true});
    this.props.firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.password)
      .then((user) => {
        user.sendEmailVerification();
        this.doNewsletterSubscriptions(this.state.email, this.state.displayName);
        // update profile information synchronously
        return user.updateProfile({displayName: this.state.displayName})
      })
      .catch(this.handleCreateError);
  }

  doNewsletterSubscriptions(email, firstName) {
    let client = new Client();

    let args = {
      data: {
        email: email,
        firstName: firstName
      },
      headers: {"Content-Type": "application/json"}
    };

    if (this.state.newsletter1) {
      args.data.newsletter = 'newsletter1';
      client.post(config.app.accountApiUrl + '/api/mail', args, (data, response) => {
        // do nothing
      });
    }

    if (this.state.newsletter2) {
      args.data.newsletter = 'newsletter2';
      client.post(config.app.accountApiUrl + '/api/mail', args, (data, response) => {
        // do nothing
      });
    }
  }

  handleAuthError(error) {
    this.setState({authenticating: false});
    console.error(error);
    if (error.code === 'auth/invalid-email' || error.code === 'auth/wrong-password' || error.code === 'auth/user-not-found') {
      this.setState({
        errors: {
          login: 'Username or password is invalid'
        }
      });
    } else if (error.code === 'auth/account-exists-with-different-credential') {
      this.setState({
        errors: {
          login: error.message
        }
      })
    } else {
      this.setState({
        errors: {
          login: `Log in failed: ${error.message}`
        }
      });
    }
  }

  handleCreateError(error) {
    this.setState({authenticating: false});
    console.error(error);
    if (error.code === 'auth/email-already-in-use') {
      this.setState({
        errors: {
          email: 'Email is already in use'
        }
      });
    } else {
      this.setState({
        errors: {
          create: `Unknown error occurred during account creation. Email support@logicahealth.org for support.`
        }
      });
    }
  }

  onCreateAccountWithGoogle() {
    this.setState({authenticating: true});
    this.props.firebase.auth().signInWithPopup(this.firebaseGoogle)
      .then((info) => {
        this.doNewsletterSubscriptions(info.user.email, info.user.displayName);
        this.redirectIfNeeded();
      })
      .catch(this.handleCreateError);
  }

  onLogInWithGoogle() {
    this.setState({authenticating: true});
    this.props.firebase.auth().signInWithPopup(this.firebaseGoogle)
      .then((user) => {
        let firebaseGoogle = this.firebaseGoogle;
        // Unlink previous provider, if one exists
        if (user.user.providerData && user.user.providerData.length !== 1) {
          user.user.providerData.forEach(function (profile) {
            if (profile.providerId !== firebaseGoogle.providerId) {
              user.user.unlink(profile.providerId).then(function() {
                // Auth provider unlinked from account
              }).catch(function(error) {
                // An error happened
              });
            }
          });
        }
        let providerEmail = user.user.providerData[0].email;
        return this.props.firebase.auth().currentUser.updateEmail(providerEmail);
      })
      .then(() => {
        this.redirectIfNeeded();
      })
      .catch(this.handleAuthError);
  }

  onTabChange = (value) => {
    this.setState({
      selectedTab: value,
      errors: {}
    });
  };

  componentWillMount() {
    switch (location.pathname) {
      case "/annon":
        this.setState({'selectedTab': 'login'});
        break;
      case "/annon/create-account":
        this.setState({'selectedTab': 'create-account'});
        break;
      default:
      //do nothing
    }
  }

  render() {
    if (this.props.currentUser) {
      return <Redirect to="/"/>
    }

    return (
      <div onKeyPress={this.handleKeyPress}>
        <Paper zDepth={2} className="login-box">
          <Tabs value={this.state.selectedTab} onChange={this.onTabChange}>
            <Tab value="login" label="LogIn">
              <div className="login-box-content">
                <LoginForm
                  authenticating={this.state.authenticating}
                  onChange={this.onChange}
                  onLogIn={this.onLogIn}
                  errors={this.state.errors}
                  email={this.state.email}
                  password={this.state.password}
                  onLogInWithGoogle={this.onLogInWithGoogle}
                />
              </div>
            </Tab>
            <Tab value="create-account" label="Create Account">
              <div className="login-box-content">
                <CreateAccountForm
                  authenticating={this.state.authenticating}
                  onChange={this.onChange}
                  onCreateAccount={this.onCreateAccount}
                  errors={this.state.errors}
                  email={this.state.email}
                  password={this.state.password}
                  displayName={this.state.displayName}
                  newsletter1={this.state.newsletter1}
                  newsletter2={this.state.newsletter2}
                  onCreateAccountWithGoogle={this.onCreateAccountWithGoogle}
                />
              </div>
            </Tab>
          </Tabs>
        </Paper>
        <div>
          <p style={{color: this.props.muiTheme.palette.textColor, fontSize: "80%"}}>First time using the new HSPC
            account system? You’ll need to <Link style={{color: this.props.muiTheme.palette.accent3Color}}
                                                 to="/annon/reset-password/first-time">reset your password</Link>.</p>
        </div>
        <Snackbar
          open={this.state.notificationOpen}
          message={this.state.notificationMessage}
          autoHideDuration={4000}
        />
      </div>
    );
  }
}

Authenticate.propTypes = {
  currentUser: PropTypes.object,
  firebase: PropTypes.object.isRequired
};

export default muiThemeable()(Authenticate);
