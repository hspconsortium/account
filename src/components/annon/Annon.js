import React from "react";
import {Redirect} from 'react-router-dom';
import Authenticate from "./Authenticate";
import ResetPassword from "./ResetPassword";
import ActionHandler from "./ActionHandler";
import './Annon.css';
import logo from '../../images/company-logo.png';
import {Route, Switch} from 'react-router-dom';
import AccountNotActivated from "./AccountNotActivated";

class Annon extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      emailSent: false,
    };

    this.sendActivationEmail = this.sendActivationEmail.bind(this);
  }

  sendActivationEmail(){
    if(this.props.currentUser) {
      this.props.currentUser.sendEmailVerification();
      this.setState({emailSent: true});
    }
  }

  render() {
    if (this.props.currentUser && this.props.currentUser.emailVerified) {
      return (<Redirect to="/"/>);
    } else if(this.props.currentUser && !this.props.currentUser.emailVerified && !window.location.pathname.includes('annon/action')) {
      return (<AccountNotActivated emailSent={this.state.emailSent} sendActivationEmail={this.sendActivationEmail}/>)
    }

    return (
      <div className="annon-content">
        <div>
          <img src={logo} className="hspc-logo" alt="HSPC Logo"/>
        </div>
        <Switch>
          <Route path="/annon/reset-password" render={() => <ResetPassword firebase={this.props.firebase}/>}/>
          <Route path="/annon/action" render={() => <ActionHandler firebase={this.props.firebase}/>}/>
          <Route path="/annon"
                 render={() => <Authenticate currentUser={this.props.currentUser} firebase={this.props.firebase}
                                             afterAuthRedirect={this.props.afterAuthRedirect}
                                             topLevelDomain={this.props.topLevelDomain}/>}/>
        </Switch>
      </div>
    );
  }
}

export default Annon;
