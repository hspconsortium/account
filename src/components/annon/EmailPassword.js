import React from 'react';
import {TextField} from "material-ui";
import PropTypes from 'prop-types';

const EmailPassword = ({email, password, onChange, errors, authenticating}) => {
  return (
    <div>
      <TextField
        floatingLabelText="Email"
        value={email}
        onChange={onChange}
        name="email"
        errorText={errors.email}
        fullWidth={true}
        disabled={authenticating}
      />
      <br/>
      <TextField
        value={password}
        floatingLabelText="Password"
        type="password"
        onChange={onChange}
        name="password"
        errorText={errors.password}
        fullWidth={true}
        disabled={authenticating}
      />
    </div>
  );
};

EmailPassword.PropTypes = {
  onChange: PropTypes.func.isRequired,
  email: PropTypes.string,
  password: PropTypes.string,
};

export default EmailPassword;
