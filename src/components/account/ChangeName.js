import React from 'react';
import {TextField} from "material-ui";

const ChangeName = ({newName, onChange, updating, errors}) => {
  return (
    <div>
      <TextField
        value={newName}
        floatingLabelText="New Name"
        onChange={onChange}
        name="newDisplayName"
        errorText={errors.message}
        disabled={updating}
        fullWidth={true}
      />
    </div>
  );
};

export default ChangeName;
