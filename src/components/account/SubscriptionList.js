import React from "react";
import {Checkbox} from "material-ui";
import PropTypes from "prop-types";
import muiThemeable from 'material-ui/styles/muiThemeable';

const SubscriptionList = ({ onChange, newsletter1, newsletter2 }) => {

  const checkboxStyle = {fontSize: '12px', marginLeft: '-10px'};

  return (
    <form className="newsletter-checkbox-form">
      <Checkbox
        className="newsletter-checkbox"
        label="Keep me up to date on HSPC Developers, Sandbox, and Gallery."
        checked={newsletter1}
        onCheck={onChange}
        labelStyle={checkboxStyle}
        name='newsletter1'
      />
      <Checkbox
        className="newsletter-checkbox"
        label="Join the HSPC mailing list."
        checked={newsletter2}
        onCheck={onChange}
        labelStyle={checkboxStyle}
        name='newsletter2'
      />
    </form>
  );
};

SubscriptionList.propTypes = {
  onChange: PropTypes.func.isRequired,
};

export default muiThemeable()(SubscriptionList);
