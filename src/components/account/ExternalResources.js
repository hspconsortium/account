import React from "react";
import PropTypes from 'prop-types';
import {Redirect} from 'react-router-dom';
import {Paper,GridList, GridTile, IconButton} from 'material-ui';
import Launch from 'material-ui/svg-icons/action/launch';
import "./Account.css";
import dev_image from '../../images/developers.png';
import gallery_image from '../../images/gallery.png';
import sandbox_image from '../../images/sandbox.png';
import hspc from '../../images/hspc.png';

class Account extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  accountLinks = [
    {
      img: sandbox_image,
      title: 'HSPC Sandbox',
      link: 'https://sandbox.logicahealth.org',
      description: 'HSPC Sandbox Manager',
    },
    {
      img: hspc,
      title: 'HSPC Main Website',
      link: 'http://logicahealth.org/',
      description: 'Healthcare Services Platform Consortium',
    },
    {
      img: dev_image,
      title: 'HSPC Developers',
      link: 'https://www.developers.logicahealth.org/',
      description: 'Information to help developers',
    },
    {
      img: gallery_image,
      title: 'HSPC Gallery',
      link: 'https://gallery.logicahealth.org',
      description: 'A gallery of apps running against the HSPC Sandbox',
    }
  ];

  render() {
    if (!this.props.currentUser) {
      return (<Redirect to="/annon"/>);
    }

    return (
      <div className="account-content">
        <Paper zDepth={2} className="account-box">
          <GridList cellHeight={256} cols={2}>
            {this.accountLinks.map((tile) => (
              <GridTile
                key={tile.img}
                title={tile.title}
                subtitle={<span>{tile.description}</span>}
                actionIcon={<a href={tile.link} target="blank" title={tile.description}><IconButton><Launch
                  color="white"/></IconButton></a>}
              >
                <img className="account-link-image" src={tile.img} alt={tile.title}/>
              </GridTile>
            ))}
          </GridList>
        </Paper>
      </div>
    );
  }
}

Account.propTypes = {
  currentUser: PropTypes.object,
};

export default Account;
