import React from 'react';
import {TextField} from "material-ui";
import PropTypes from 'prop-types';
import Constants from'../shared/Constants'

class NewPassword extends React.Component {
  constructor(props) {
    super(props);

    this._newPassword= '';
    this._confirmPassword= '';

    this.state = {
      errors: {}
    };
  }

  _validatePassword() {
    this.props.onValidatePassword(false);
    if (0 < this._newPassword.length && this._newPassword.length < Constants.MIN_PASSWORD_LENGTH ) {
      this.setState({errors: {
        length: `Password must be at least ${Constants.MIN_PASSWORD_LENGTH} characters long`,
        match: ''
      }});
    } else {
      this.setState({errors: {
        length: ''
      }});
      if (0 < this._confirmPassword.length && this._newPassword !== this._confirmPassword) {
        this.setState({errors: {
          match: 'Password do not match'
        }});
      } else if (this._newPassword === this._confirmPassword) {
        this.setState({errors: {
          length: '',
          match: ''
        }});
        this.props.onValidatePassword(true);
      }
    }
  };

  _onChange = (event) => {
    if (event.target.name === 'newPassword')
      this._newPassword = event.target.value;
    else if (event.target.name === 'confirmPassword')
      this._confirmPassword = event.target.value;
    this._validatePassword();
    this.props.onChange(event);
  };

  render() {
    return (
      <div>
        <TextField
          value={this._newPassword}
          floatingLabelText="New password"
          type="password"
          onChange={this._onChange}
          name="newPassword"
          errorText={this.state.errors.length}
          fullWidth={true}
          disabled={this.props.updating}
          autoComplete="new-password"
        />
        <br/>
        <TextField
          value={this._confirmPassword}
          floatingLabelText="Confirm new password"
          type="password"
          onChange={this._onChange}
          name="confirmPassword"
          errorText={this.state.errors.match}
          fullWidth={true}
          disabled={this.props.updating}
          autoComplete="new-password"
        />
      </div>
    );
  }
}

NewPassword.PropTypes = {
  updating: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
  onValidatePassword: PropTypes.func.isRequired,
};

export default NewPassword;
