import React from 'react';
import {MuiThemeProvider} from "material-ui";
import muiTheme from '../../config/muiThemeConfig';
import {FlatButton, Dialog, IconButton, FontIcon} from "material-ui";
import EditorModeEdit from 'material-ui/svg-icons/editor/mode-edit';
import PropTypes from 'prop-types';
import ChangePassword from "./ChangePassword";

class PasswordSetting extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      open: false,
      canSubmit: false,
      validPassword: false
    };

    this.onValidatePassword = this.onValidatePassword.bind(this);
  }

  onValidatePassword(valid) {
    if (valid && this.props.curPassword.length > 0) {
      this.setState({canSubmit: true});
    } else {
      this.setState({canSubmit: false});
    }
  };

  getProvider = () => {
    // This always fails as the user changes their auth provider, but succeeds upon reloading the page
    // if (!this.props.currentUser.providerData || this.props.currentUser.providerData.length !== 1) {
    //   throw new Error("Bad provider data for current user: " + this.props.currentUser);
    // }

    return this.props.currentUser.providerData[0].providerId;
  };

  handleOpen = () => {
    this.setState({open: true});
  };

  handleClose = () => {
    this.props.onCancel();
    this.setState({open: false});
  };

  handleSubmit = () => {
    this.props.onUpdatePassword();
    this.setState({open: false});
  };

  render() {
    const actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onClick={this.handleClose}
      />,
      <FlatButton
        label="Submit"
        primary={true}
        disabled={!this.state.canSubmit}
        onClick={this.handleSubmit}
      />,
    ];

    return (
      <div className="account-settings-box" style={{display: 'flex'}}>
        <div className="account-settings-label">Password</div>
        <div style={{flexGrow: 1}}>**********</div>

        {this.getProvider() === 'password' &&
          <IconButton style={{width: 'auto'}}>
            <EditorModeEdit onClick={this.handleOpen}/>
          </IconButton>
        }

        <MuiThemeProvider muiTheme={muiTheme}>
          {this.getProvider() === 'google.com' &&
          <FontIcon title="Password is managed with your Google account." color={muiTheme.palette.disabledColor} style={{width: 'auto'}} className="fa fa-google"/>
          }
        </MuiThemeProvider>

        <Dialog
          title="Change Password"
          contentStyle={{width: "400px", maxWidth: "400px"}}
          actions={actions}
          modal={true}
          open={this.state.open || !this.props.success}
        >
          <ChangePassword
            curPassword={this.props.curPassword}
            newPassword={this.props.newPassword}
            errors={this.props.errors}
            onValidatePassword={this.onValidatePassword}
            onChange={this.props.onChange}/>
        </Dialog>
      </div>
    );
  }
}

PasswordSetting.PropTypes = {
  onChange: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
  onUpdatePassword: PropTypes.func.isRequired,
  curPassword: PropTypes.string,
  newPassword: PropTypes.string,
  errors: PropTypes.object,
  currentUser: PropTypes.object.isRequired
};

export default PasswordSetting;
