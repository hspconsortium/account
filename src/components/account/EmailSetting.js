import React from 'react';
import {MuiThemeProvider} from "material-ui";
import muiTheme from '../../config/muiThemeConfig';
import {FlatButton, Dialog, IconButton, FontIcon} from "material-ui";
import EditorModeEdit from 'material-ui/svg-icons/editor/mode-edit';
import PropTypes from 'prop-types';
import ChangeEmail from "./ChangeEmail";

class EmailSetting extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      open: false,
      errors: {message: props.errors.message}
    };

    this._onChange = this._onChange.bind(this);
  }

  getProvider = () => {
    // This always fails as the user changes their auth provider, but succeeds upon reloading the page
    // if (!this.props.currentUser.providerData || this.props.currentUser.providerData.length !== 1) {
    //   throw new Error("Bad provider data for current user: " + this.props.currentUser);
    // }

    return this.props.currentUser.providerData[0].providerId;
  };

  validEmail = (email) => {
    // eslint-disable-next-line
    let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  };

  handleOpen = () => {
    this.setState({open: true});
  };

  handleClose = () => {
    this.props.onCancel();
    this.setState({open: false});
  };

  handleSubmit = () => {
    this.props.onUpdateEmail();
    this.setState({open: false});
  };

  _onChange(event) {
    this.props.onChange(event);
    if (event.target.name === 'newEmail') {
      if (this.validEmail(event.target.value)) {
        this.setState({errors: {message: ''}});
      } else {
        this.setState({errors: {message: 'Enter a valid email'}});
      }
    }
  };

  render() {
    const actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onClick={this.handleClose}
      />,
      <FlatButton
        label="Submit"
        primary={true}
        disabled={!this.validEmail(this.props.newEmail)}
        onClick={this.handleSubmit}
      />
    ];

    return (
      <div className="account-settings-box" style={{display: 'flex'}}>
        <div className="account-settings-label">Email</div>
        <div style={{flexGrow: 1}}>{this.props.currentUser.email}</div>

        {this.getProvider() === 'password' &&
        <IconButton style={{width: 'auto'}}>
          <EditorModeEdit onClick={this.handleOpen}/>
        </IconButton>}

        <MuiThemeProvider muiTheme={muiTheme}>
          {this.getProvider() === 'google.com' &&
            <FontIcon title="Email is managed with your Google account." color={muiTheme.palette.disabledColor} style={{width: 'auto'}} className="fa fa-google"/>
          }
        </MuiThemeProvider>

        <Dialog
          title="Change Email"
          contentStyle={{width: "400px", maxWidth: "400px"}}
          actions={actions}
          modal={true}
          open={this.state.open || !this.props.success}
        >
          <ChangeEmail
            newEmail={this.props.newEmail}
            onChange={this._onChange}
            errors={this.state.errors}
            updateErrors={this.props.errors}/>
        </Dialog>
      </div>
    );
  }
}

EmailSetting.PropTypes = {
  currentUser: PropTypes.object,
  newEmail: PropTypes.string,
  updating: PropTypes.bool,
  success: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
  onUpdateEmail: PropTypes.func.isRequired,
  errors: PropTypes.object
};

export default EmailSetting;
