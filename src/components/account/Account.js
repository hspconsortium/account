import React from "react";
import * as Firebase from "firebase";
import PropTypes from 'prop-types';
import {Redirect} from 'react-router-dom';
import {Paper, Divider, Snackbar} from 'material-ui';
import NameSetting from "./NameSetting";
import EmailSetting from "./EmailSetting";
import PasswordSetting from "./PasswordSetting";
import SubscriptionList from "./SubscriptionList";
import "./Account.css";
import {Client} from "node-rest-client";
import config from "../../config/config";

class Account extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      newPassword: '',
      curPassword: '',
      password: '',
      newEmail: this.props.currentUser ? this.props.currentUser.email : '',
      newDisplayName: this.props.currentUser ? this.props.currentUser.displayName  : '',
      updating: false,
      errors: {message: ''},
      nameSuccess: true,
      emailSuccess: true,
      passwordSuccess: true,
      loginSuccess: true,
      loginOpen: false,
      newsletter1: false,
      newsletter2: false,
      notificationMessageOut: "You have opted out of emails for this list.",
      notificationMessageIn: "You have successfully signed up to receive emails for this list.",
      notificationOpenOut: false,
      notificationOpenIn: false
    };

    this.onChange = this.onChange.bind(this);
    this.changeSubscription = this.changeSubscription.bind(this);
    this.onCancel = this.onCancel.bind(this);
    this.onUpdateUsername = this.onUpdateUsername.bind(this);
    this.onUpdateEmail = this.onUpdateEmail.bind(this);
    this.onUpdatePassword = this.onUpdatePassword.bind(this);
    this.checkNewsletterStatus(props);
  }

  /* Check which (if any) newsletters the user is subscribed to and change the checkboxes */
  checkNewsletterStatus(props) {
    if(props.currentUser==null) return;
    var email = props.currentUser.email;
    let client = new Client();
    let args = {
      data: {
        email: email
      },
      headers: {"Content-Type": "application/json"}
    };
    client.post(config.app.accountApiUrl + '/api/mail/status', args, (data, response) => {
      data = JSON.parse(data)
      if(data[0] === 'true') this.setState({newsletter1: !this.state.newsletter1});
      if(data[1] === 'true') this.setState({newsletter2: !this.state.newsletter2});
    });
  }

   onChange(event) {
    if (event.target.name === 'curPassword')
      this.setState({curPassword: event.target.value});
    else if (event.target.name === 'newPassword')
      this.setState({newPassword: event.target.value});
    else if (event.target.name === 'newEmail')
      this.setState({newEmail: event.target.value});
    else if (event.target.name === 'newDisplayName')
      this.setState({newDisplayName: event.target.value});
    else if (event.target.name === 'newsletter1'){
      this.setState({newsletter1: !this.state.newsletter1});
      this.setState({notificationOpenOut: this.state.newsletter1})
      this.setState({notificationOpenIn: !this.state.newsletter1})
      this.changeSubscription('newsletter1');
    }
    else if (event.target.name === 'newsletter2'){
      this.setState({newsletter2: !this.state.newsletter2});
      this.setState({notificationOpenOut: this.state.newsletter2})
      this.setState({notificationOpenIn: !this.state.newsletter2})
      this.changeSubscription('newsletter2');
    }
  };

  _resetState() {
    this.setState({newEmail: this.props.currentUser.email});
    this.setState({newDisplayName: this.props.currentUser.displayName});
    this.setState({newPassword: ''});
    this.setState({curPassword: ''});
    this.setState({password: ''});
    this.clearFailures();
    this.clearMessage();
  };

  onCancel() {
    this._resetState();
  };

  changeSubscription(newsletter) {
    let client = new Client();
    let email = this.props.currentUser.email;
    let firstName = this.props.currentUser.displayName.split(' ')[0];
    let lastName = this.props.currentUser.displayName.split(' ')[1];
    let args = {
      data: {
        email: email,
        firstName: firstName,
        lastName: lastName
      },
      headers: {"Content-Type": "application/json"}
    };
    if (newsletter === 'newsletter1'&&!this.state.newsletter1) {
      args.data.newsletter = 'newsletter1';
      client.post(config.app.accountApiUrl + '/api/mail', args, (data, response) => {
        // do nothing
      });
    }
    if (newsletter === 'newsletter2'&&!this.state.newsletter2) {
      args.data.newsletter = 'newsletter2';
      client.post(config.app.accountApiUrl + '/api/mail', args, (data, response) => {
        // do nothing
      });
    }
    if(newsletter === 'newsletter1'&&this.state.newsletter1) {
      args.data.newsletter = 'newsletter1';
      client.patch(config.app.accountApiUrl + '/api/mail', args, (data, response) => {
        // do nothing
      });
    }
    if(newsletter === 'newsletter2'&&this.state.newsletter2) {
      args.data.newsletter = 'newsletter2';
      client.patch(config.app.accountApiUrl + '/api/mail', args, (data, response) => {
        // do nothing
      });
    }
  };

  handleClose = () => {
    this.setState({password: ''});
    this.setState({errors: {message: ''}});
    this.setState({loginSuccess: true});
    this.setState({loginOpen: false});
  };

  render() {
    // only make available to authenticated users
    if (!this.props.currentUser || !this.props.currentUser.emailVerified) {
      return (<Redirect to="/annon"/>);
    }

    return (
      <div className="account-content">
        <Paper zDepth={2} className="account-box">
          <NameSetting
            currentUser={this.props.currentUser}
            newDisplayName={this.state.newDisplayName}
            updating={this.state.updating}
            errors={this.state.errors}
            success={this.state.nameSuccess}
            onChange={this.onChange}
            onCancel={this.onCancel}
            onUpdateUsername={this.onUpdateUsername}
          />
          <Divider />
          <EmailSetting
            currentUser={this.props.currentUser}
            newEmail={this.state.newEmail}
            updating={this.state.updating}
            errors={this.state.errors}
            success={this.state.emailSuccess}
            onChange={this.onChange}
            onCancel={this.onCancel}
            onUpdateEmail={this.onUpdateEmail}
          />
          <Divider />
          <PasswordSetting
            currentUser={this.props.currentUser}
            newPassword={this.state.newPassword}
            curPassword={this.state.curPassword}
            updating={this.state.updating}
            errors={this.state.errors}
            success={this.state.passwordSuccess}
            onChange={this.onChange}
            onCancel={this.onCancel}
            onUpdatePassword={this.onUpdatePassword}
          />
          <Divider />
          <SubscriptionList
            onChange={this.onChange}
            onClick={this.onClick}
            newsletter1={this.state.newsletter1}
            newsletter2={this.state.newsletter2}
          />
          <Snackbar
            open={this.state.notificationOpenIn}
            message={this.state.notificationMessageIn}
            autoHideDuration={3000}
          />
          <Snackbar
            open={this.state.notificationOpenOut}
            message={this.state.notificationMessageOut}
            autoHideDuration={3000}
          />
        </Paper>
      </div>
    );
  }

  clearFailures() {
    this.setState({nameSuccess: true});
    this.setState({emailSuccess: true});
    this.setState({passwordSuccess: true});
    this.setState({loginSuccess: true});
    this.setState({loginOpen: false});
    this.setState({errors: {message: ''}});
  }

  clearMessage() {
    this.setState({errors: {message: ''}});
  }

  onUpdateUsername() {
    this.setState({nameSuccess: false});
    this.setState({updating: true});
    this.handleUpdateName();
    this.setState({updating: false});
  }
  onUpdateEmail() {
    this.setState({emailSuccess: false});
    this.setState({updating: true});
    this.handleUpdateEmail();
    this.setState({updating: false});
  }
  onUpdatePassword() {
    this.setState({passwordSuccess: false});
    this.setState({updating: true});
    this.handleUpdatePassword();
    this.setState({updating: false});
  }

  handleUpdateName() {
    this.props.firebase.auth().currentUser.updateProfile({displayName: this.state.newDisplayName})
      .then(() => {
        this._resetState();
      })
      .then(() => {
        return this.verifyUserProfileInfo()
      })
      .catch(this.handleError);
  }

  handleUpdateEmail() {
    const user = this.props.firebase.auth().currentUser;
    const credential = Firebase.auth.EmailAuthProvider.credential(
      this.props.currentUser.email,
      this.state.curPassword
    );

    user.reauthenticate(credential)
      .then(() => {
        this.props.firebase.auth().currentUser.updateEmail(this.state.newEmail)
          .then(() => {
            this.props.firebase.auth().currentUser.sendEmailVerification();
            this._resetState();
          })
          .then(() => {
            return this.verifyUserProfileInfo()
          })
          .catch(this.handleError);
      })
      .catch(this.handleError);
  }

  handleUpdatePassword() {
    const user = this.props.firebase.auth().currentUser;
    const credential = Firebase.auth.EmailAuthProvider.credential(
      user.email,
      this.state.curPassword
    );

    user.reauthenticate(credential)
      .then(() => {
        user.updatePassword(this.state.newPassword)
          .then(() => {
            this._resetState();
          })
          .then(() => {
            return this.verifyUserProfileInfo()
          })
          .catch(this.handleError);
      })
      .catch(this.handleError);
  }

  verifyUserProfileInfo() {
    // copy profile info to database for a richer and more accessible user profile database
    let user = this.props.firebase.auth().currentUser;
    return this.props.firebase.database().ref('users/' + user.uid).set({
      displayName: user.displayName,
      email: user.email,
      uid: user.uid,
    });
  }

  handleError = (error) => {
    this.setState({updating: false});
    console.error(error.message);
    if (error.code === 'auth/requires-recent-login') {
      this.setState({loginOpen: true});
    } else if (error.code === 'auth/invalid-email' ||
      error.code === 'auth/wrong-password' ||
      error.code === 'auth/user-not-found') {
      this.setState({
        errors: {
          message: 'Password is invalid'
        }
      });
    } else {
      this.setState({
        errors: {
          message: `Account update failed: ${error.message}`
        }
      });
    }
  }

}

Account.propTypes = {
  currentUser: PropTypes.object,
  firebase: PropTypes.object.isRequired
};

export default Account;
