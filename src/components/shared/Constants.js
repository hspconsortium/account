const Constants = {
  MIN_PASSWORD_LENGTH: 6
};

export default Constants;
