import React from 'react';
import PropTypes from 'prop-types';
import {AppBar, FlatButton, IconButton, Popover, Menu, MenuItem} from 'material-ui';
import NavigationApps from 'material-ui/svg-icons/navigation/apps';
import muiThemeable from 'material-ui/styles/muiThemeable';
import "./Header.css";
import dev_image from '../../images/hspc-developers-icon@3x.png';
import gallery_image from '../../images/hspc-gallery-icon@3x.png';
import sandbox_image from '../../images/hspc-sndbx-icon@3x.png';
import hspc from '../../images/hspc-logo-md.png';

class Header extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      openApps: false,
      openName: false
    };
  }

  handleOpenApps = (event) => {
    event.preventDefault();

    this.setState({
      openApps: true,
      anchorElApps: event.currentTarget,
    });
  };

  handleCloseApps = () => {
    this.setState({openApps: false});
  };

  handleOpenName = (event) => {
    event.preventDefault();

    this.setState({
      openName: true,
      anchorElName: event.currentTarget,
    });
  };

  handleCloseName = () => {
    this.setState({openName: false});
  };

  accountLinks = [
    {
      img: hspc,
      title: 'Consortium',
      link: 'http://logicahealth.org/',
    },
    {
      img: sandbox_image,
      title: 'Sandbox',
      link: 'https://sandbox.logicahealth.org',
    },
    {
      img: dev_image,
      title: 'Developers',
      link: 'https://www.developers.logicahealth.org/',
    },
    {
      img: gallery_image,
      title: 'Gallery',
      link: 'https://gallery.logicahealth.org',
    }
  ];

  render() {
    const buttonStyle = ({
      color: this.props.muiTheme.palette.canvasColor,
      textTransform: "uppercase",
      marginLeft: "10px",
      marginTop: "14px",
      verticalAlign: "middle",
      letterSpacing: 0,
      fontWeight: 500,
      fontSize: "14px"
    });

    const rightButtons = (
      <div>
        <IconButton>
          <NavigationApps color={this.props.muiTheme.palette.canvasColor} onClick={this.handleOpenApps}/>
        </IconButton>
        <Popover
          open={this.state.openApps}
          anchorEl={this.state.anchorElApps}
          anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
          targetOrigin={{horizontal: 'left', vertical: 'top'}}
          onRequestClose={this.handleCloseApps}
        >
          <Menu>
            {this.accountLinks.map((tile) => (
              <MenuItem style={{lineHeight: 1, textAlign: 'center'}} primaryText={tile.title} key={tile.img}>
                <a className="nav-link" href={tile.link}>
                  <img className="nav-link-image" src={tile.img} alt={tile.title}/>
                </a>
              </MenuItem>
            ))}
          </Menu>
        </Popover>
      </div>
    );

    return (
      <AppBar
        title='HSPC Account'
        iconElementRight={rightButtons}
        showMenuIconButton={false}
      >
        <FlatButton style={buttonStyle} className="display-name" onClick={this.handleOpenName}
                    label={this.props.currentUser.displayName || this.props.currentUser.email}/>
        <Popover
          open={this.state.openName}
          anchorEl={this.state.anchorElName}
          anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
          targetOrigin={{horizontal: 'left', vertical: 'top'}}
          onRequestClose={this.handleCloseName}
        >
          <Menu>
            <MenuItem primaryText="Sign out" key="sign-out" onTouchTap={this.props.onSignOut}/>
          </Menu>
        </Popover>
      </AppBar>
    );
  }
}

Header.propTypes = {
  onSignOut: PropTypes.func.isRequired,
  currentUser: PropTypes.object.isRequired,
  muiTheme: PropTypes.object.isRequired
};
Header.defaultProps = {};

export default muiThemeable()(Header);
