import React from 'react';
import {Redirect} from 'react-router-dom';

function Default(props) {
  // if we have content we want to display to auth or annon users we can put that here, but for now just
  // redirect to the account screen.
  return (
    <div>
      <Redirect to="/account"/>
    </div>
  );
}

export default Default;
