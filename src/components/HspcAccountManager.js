import React from "react";
import injectTapEventPlugin from "react-tap-event-plugin";
import {MuiThemeProvider} from "material-ui";
import * as Firebase from "firebase";
import config from "../config/config";
import PropTypes from "prop-types";
import Account from "./account/Account";
import {Route, BrowserRouter as Router, Switch, Redirect} from "react-router-dom";
import Annon from "./annon/Annon";
import LoadingFrame from "./shared/LoadingFrame";
import Default from "./default/Default";
import "./HspcAccountManager.css";
import Header from './shared/Header';
import muiTheme from '../config/muiThemeConfig';
import 'font-awesome/css/font-awesome.min.css';
import Cookies from 'universal-cookie';
import qs from 'querystring';

// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin();

class HspcAccountManager extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      currentUser: undefined,
      afterAuthRedirect: null,
      afterLogoutRedirect: null,
      redirecting: false
    };

    this.updateCurrentUser = this.updateCurrentUser.bind(this);
    this.onSignOut = this.onSignOut.bind(this);
    this.handleNav = this.handleNav.bind(this);
    this.checkForRedirect = this.checkForRedirect.bind(this);

    // set up firebase
    this.firebase = Firebase.initializeApp(config.firebase);
    this.firebase.auth().onAuthStateChanged(this.updateCurrentUser);
  }

  updateCurrentUser(newUser) {
    this.setState({currentUser: newUser});
    const cookies = new Cookies();

    if (newUser && newUser.emailVerified) {
      newUser.getToken().then((token) => {
        cookies.set('hspc-token', token, {
          domain: this.getTopLevelDomain(),
          path: '/'
        });
      });
    } else {
      cookies.set('hspc-token', 'expired', {
        expires: new Date(),
        domain: this.getTopLevelDomain(),
        path: '/'
      });
    }
  }

  getTopLevelDomain() {
    if (window.location.hostname === 'localhost')
      return 'localhost';

    let domains = window.location.hostname.split('.');
    return `${domains[domains.length - 2]}.${domains[domains.length - 1]}`;
  }

  checkForRedirect() {
    let queryString = window.location.search;
    if (queryString) {
      let query = qs.parse(queryString.substring(1));
      if (query.afterAuth) {
        this.setState({
          afterAuthRedirect: query.afterAuth
        })
      } else if (query.afterLogout) {
        this.setState({
          afterLogoutRedirect: query.afterLogout
        })
      }
    }
  }

  deleteCookies(){
    var cookies = document.cookie.split(";");

    document.cookie = "hspc-persona-token=; path=/; domain=.logicahealth.org; expires=Thu, 18 Dec 2013 12:00:00 UTC; ";

    for (var i = 0; i < cookies.length; i++) {

      var cookie = cookies[i];
      console.log("the cookie is: " + cookie);
      var eqPos = cookie.indexOf("=");
      var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
      document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
    }

  }

  onSignOut() {
    this.firebase.auth().signOut();
    if (this.state.afterLogoutRedirect)
      window.location = this.state.afterLogoutRedirect;
    return <Redirect to="/annon"/>
  }

  handleNav() {
    this.firebase.auth().signOut();
    if (this.state.afterLogoutRedirect)
      window.location = this.state.afterLogoutRedirect;
    return <Redirect to="/annon"/>
  }

  componentWillMount() {
    this.checkForRedirect();
  }

  render() {
    // Wait until we know if the user is authenticated before we start rendering everything.
    if (this.state.currentUser === undefined) {
      return (<LoadingFrame/>);
    }

    return (
      <MuiThemeProvider muiTheme={muiTheme}>
        <Router>
          <div>
            {this.state.currentUser && <Header currentUser={this.state.currentUser} onSignOut={this.onSignOut} handleNav={this.handleNav}/>}
            <div className="content">
              <Switch>
                // temporary fix for bad sandbox link path
                <Route path="/public/newuser/" exact render={() => <Redirect to="/annon/create-account"/>}/>
                <Route exact path="/" component={Default}/>
                <Route exact path="/account/logout" render={() => {
                  this.deleteCookies();
                  return this.onSignOut();
                }}/>
                <Route
                  path="/account"
                  render={() => {
                    // this handles the situation where a user is already authenticated, and needs to be redirected
                    // back after a token refresh.
                    if(this.state.currentUser && this.state.currentUser.emailVerified && this.state.afterAuthRedirect) {
                      window.location = this.state.afterAuthRedirect;
                      return (<LoadingFrame/>);
                    }
                    else {
                      return <Account currentUser={this.state.currentUser} firebase={this.firebase}/>
                    }
                  }}
                />
                <Route
                  path="/annon"
                  render={() =>
                    <Annon currentUser={this.state.currentUser} firebase={this.firebase}
                           afterAuthRedirect={this.state.afterAuthRedirect} topLevelDomain={this.getTopLevelDomain()}/>
                  }
                />
                <Route path="/" component={Default}/>
              </Switch>
            </div>
          </div>
        </Router>
      </MuiThemeProvider>
    );
  }
}

HspcAccountManager.contextTypes = {
  router: PropTypes.object
};

export default HspcAccountManager;
