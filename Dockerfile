FROM node:alpine

COPY . .
RUN npm install
RUN npm run build
CMD ["node", "./node_modules/serve/bin/serve.js", "-s", "-p", "8065", "build" ]
